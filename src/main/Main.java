package main;

import lab.*;
import project.*;

/**
 * Main
 *
 * @author jacob
 */
public class Main {

	/**
	 * Program entry point
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		// Run Project 4
		Project4.test();
	}
}
