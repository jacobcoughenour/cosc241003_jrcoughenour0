package lab;

import java.util.Arrays;
import java.util.Vector;

/**
 * Lab 1 test class
 * @author jacob
 */
public class Lab1 {

	/**
	 * Vector test
	 * @param args
	 */
	public static void test(String[] args) {

		Vector<Object> vector = new Vector<Object>();

		// create some objects
		int primitiveInt = 241;
		Integer wrapperInt = new Integer(2048);
		String str = "Jacob Coughenour";

		// add objects to vector
		vector.add(primitiveInt);
		vector.add(wrapperInt);
		vector.add(str);
		vector.add(2, new Integer(2188));

		System.out.println("The elements of vector: " + vector);
		System.out.println("The size of vector is: " + vector.size());
		System.out.println("The elements at position 2 is: " + vector.elementAt(2));
		System.out.println("The first element of vector is: " + vector.firstElement());
		System.out.println("The last element of vector is: " + vector.lastElement());

		vector.removeElementAt(1);

		System.out.println("The elements of vector: " + vector);
		System.out.println("The size of vector is: " + vector.size());
		System.out.println("The elements at position 2 is: " + vector.elementAt(2));
		System.out.println("The first element of vector is: " + vector.firstElement());
		System.out.println("The last element of vector is: " + vector.lastElement());

		vector.clear();

		// add all args to vector
		vector.addAll(Arrays.asList(args)); // netbeans told me to do this instead of for-each

		System.out.println("The elements of vector: " + vector);

		// remove all odd indices
		for (int i = 1; i < vector.size(); i++)
			vector.removeElementAt(i);

		System.out.println("The elements of vector: " + vector);

	}

}
