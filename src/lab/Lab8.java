package lab;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import collection.MyDeque;

/**
 * Lab 8
 * @author jacob
 */
public class Lab8 {

	/**
	 * Lab 8 test
	 */
	public static void test() {

		try {

			// get input file
			Scanner wordsScanner = new Scanner(new File("COSC241_L8_Input.txt"));

			// create output file
			PrintWriter outputFile = new PrintWriter("COSC241_L8_Output_jrcoughenour0.txt", "UTF-8");

			// loop through each line in the file
			while (wordsScanner.hasNextLine()) {

				// get next word
				// trim ends of string
				// split into an array of chars
				// cast to object array
				// construct a new queue from object array
				MyDeque simpleDeque = new MyDeque((Object[]) wordsScanner.nextLine().trim().split(""));

				// while deque has more than 1 char left
				// pop the first and last chars off the deque
				// and continue if they are equal
				while (simpleDeque.size() > 1 && simpleDeque.popFirst().equals(simpleDeque.popLast()));

				// if its a palindrome, the deque should have 0 or 1 chars left
				// write true if 0 or 1 chars left and false otherwise
				outputFile.println(simpleDeque.size() < 2 ? "true" : "false");
			}

			// close the read/write streams
			wordsScanner.close();
			outputFile.close();

		} catch (Exception e) {
			System.err.println(e);
		}

	}

}