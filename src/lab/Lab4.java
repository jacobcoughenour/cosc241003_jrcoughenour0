package lab;

import java.util.Scanner;

/**
 * Lab 4
 * @author jacob
 */
public class Lab4 {

	public static void test() {

		// setup scanner
		Scanner r = new Scanner(System.in);
		String input = "";

		do {
			System.out.print("Enter a string: ");

			// read input
			input = r.nextLine();

			// if empty
			if (input.equals(""))
				break;

			// print whether input is a plaindrome
			System.out.print(input);
			System.out.print(isPalindrome(input) ? " is" : " is not");
			System.out.println(" a palindrome.");

			// exit if empty
		} while (!input.equals(""));

		System.out.println("exited");

	}

	/**
	 * Determines whether the given string is a palindrome
	 * @param s input string
	 * @return true if input string is a palindrome
	 */
	public static Boolean isPalindrome(String s) {

		// string is smaller than 2 chars
		if (s.length() < 2)
			return true;

		// if first char equals last char
		if (Character.toLowerCase(s.charAt(0)) == Character.toLowerCase(s.charAt(s.length() - 1)))
			// pass string without first and last char
			return isPalindrome(s.substring(1, s.length() - 1));

		// chars don't match; s is not a palindrome
		return false;
	}
}
