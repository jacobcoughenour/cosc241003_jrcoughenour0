package lab;

import collection.MyVector;

/**
 * Lab2 test class
 * @author jacob
 */
public class Lab2 {

	public static void test() {

		MyVector vector = new MyVector();

		// fibonacci sequence
		int a = 1;
		int b = 0;
		int temp = 0;
		for (int i = 0; i < 25; i++) {
			vector.append(temp);
			temp = a;
			a += b;
			b = temp;
		}
		System.out.println(vector);

		// reverse vector
		vector.reverse();
		System.out.println(vector);

		// create clone of vector
		MyVector clone = vector.clone();

		// remove odd indicies
		for (int i = 1; i < vector.size(); i++)
			vector.removeAt(i);

		System.out.println(vector);

		// reverse cloned
		clone.reverse();
		System.out.println(clone);

		// merge
		vector.merge(clone);
		System.out.println(vector);
	}
}
