package lab;

import collection.SLList;
import collection.SortedSLList;

/**
 * Lab 6
 * @author jacob
 */
public class Lab6 {

	/**
	 * Lab 6 test
	 */
	public static void test() {

		SLList simpleList = new SLList();

		for (int i = 1; i < 31; i++) {
			if (i % 2 == 0)
				simpleList.append(i * i);
			else
				simpleList.insert(i * i);
		}

		System.out.println(simpleList);
		System.out.println("size " + simpleList.size());
		System.out.println();

		simpleList.remove(25, 36, 64, 100, 400);

		System.out.println(simpleList);
		System.out.println("size " + simpleList.size());
		System.out.println();

		SortedSLList sortedList = new SortedSLList();

		for (int i = 1; i < 31; i++)
			sortedList.insert(i * i);

		System.out.println(sortedList);
		System.out.println("size " + sortedList.size());
		System.out.println();

		sortedList.remove(1, 25, 36, 64, 144, 400, 900);

		System.out.println(sortedList);
		System.out.println("size " + sortedList.size());
		System.out.println();

		sortedList.insert(1, 64, 400, 900);

		System.out.println(sortedList);
		System.out.println("size " + sortedList.size());
	}
}
