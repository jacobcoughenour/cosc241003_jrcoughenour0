package lab;

import java.util.Random;
import java.io.PrintWriter;

import collection.MyStack;
import collection.MyQueue;

/**
 * Lab 7
 * @author jacob
 */
public class Lab7 {

	/**
	 * Lab 7 test
	 */
	public static void test() {

		// create stack and queue
		MyStack stack = new MyStack();
		MyQueue queue = new MyQueue();

		Random rand = new Random(System.nanoTime());

		// add 60 random numbers to stack and queue
		for (int i = 0; i < 60; i++) {
			int num = rand.nextInt();
			stack.push(num);
			queue.insertBack(num);
		}

		// remove 30 numbers from stack and queue
		for (int i = 0; i < 30; i++) {
			stack.pop();
			queue.removeFront();
		}

		// write stack and queue contents to file
		try {
			PrintWriter file = new PrintWriter("COSC241_L7_Output_jrcoughenour0.txt", "UTF-8");

			file.println(stack);
			file.println(queue);

			file.close();
		} catch (Exception e) {

		}


	}
}