package lab;

import java.util.Random;

import collection.MyBinarySearchTree;
import collection.MyBinaryTree.Order;

/**
 * Lab 9
 *
 * @author jacob
 */
public class Lab9 {

	/**
	 * Lab 9 test
	 */
	public static void test() {

		// create binary search tree
		MyBinarySearchTree<Integer> tree = new MyBinarySearchTree<Integer>();

		// insert 100, 50, and 150
		tree.insert(100, 50, 150);

		Random rand = new Random(System.nanoTime());

		// add 50 random numbers to tree
		for (int i = 0; i < 50; i++)
			tree.insert(rand.nextInt(300));

		// print out tree inorder, preorder, and postorder
		System.out.println("Inorder");
		System.out.println(tree);
		System.out.println();

		System.out.println("Preorder");
		System.out.println(tree.toString(Order.Preorder));
		System.out.println();

		System.out.println("Postorder");
		System.out.println(tree.toString(Order.Postorder));
		System.out.println();

		// remove smallest, largest, 50, 100, and 150
		tree.remove(tree.getMin(), tree.getMax(), 50, 100, 150);
		System.out.println("removed smallest, largest, 50, 100, and 150");
		System.out.println();

		// print out tree inorder, preorder, and postorder
		System.out.println("Inorder");
		System.out.println(tree);
		System.out.println();

		System.out.println("Preorder");
		System.out.println(tree.toString(Order.Preorder));
		System.out.println();

		System.out.println("Postorder");
		System.out.println(tree.toString(Order.Postorder));

	}
}