package lab;

import java.util.Random;
import java.util.ArrayList;
import java.lang.reflect.Method;

import collection.MySort;
import collection.MyVector;

/**
 * Lab 5
 * @author jacob
 */
public class Lab5 {

	// how many numbers to generate
	private static final int SIZE = 30000;

	// range of random numbers
	private static final int RANGE = 100000;

	// number of tests to run per sorting method
	private static final int TESTS = 3;

	// indecies to display from the output of each method
	private static final int[] INDICIES = new int[] { 0, 1, 2, 9999, 19999, 29999 };

	/**
	 * Tests all the sorting methods in collection.MySort using reflection. <br>
	 * <br>
	 * Each method is:
	 * <ul>
	 * <li>Given a cloned vector of the same length of random unsorted numbers</li>
	 * <li>Expected to modify the given unsorted vector (not return a new sorted vector)</li>
	 * <li>Tested a set number of times to get an overall average time</li>
	 * </ul>
	 * <br>
	 * This is stupidly over-engineered... sorry
	 */
	public static void test() {


		MyVector correct = new MyVector(SIZE);

		// generate random numbers from set seed
		Random rand = new Random(20181010);
		while (correct.size() < SIZE)
			correct.append(rand.nextInt(RANGE));

		// clone vector before we sort it so that we can have an unsorted vector for test cases
		MyVector unsorted = correct.clone();

		// sort vector to make it correct so we can compare it in tests
		MySort.selectionSort(correct);

		long startTime, endTime;


		// output table header

		System.out.print("Method\tTime\t");
		for (int i : INDICIES)
			if (i < SIZE) {
				if (i == 1)
					System.out.print(i + "st\t");
				else if (i == 2)
					System.out.print(i + "nd\t");
				else
					System.out.print(i + "th\t");
			}
		System.out.println("\n-------------------------------------------------------------");



		try {

			// get all the methods from MySort
			Method[] methods = Class.forName("collection.MySort").getMethods();

			// create a list with only the Methods that contain "Sort" in their name
			ArrayList<Method> sortMethods = new ArrayList<Method>();
			for (int i = 0; i < methods.length; i++)
				if (methods[i].getName().contains("Sort"))
					sortMethods.add(methods[i]);


			// Test each sort method

			for (Method m : sortMethods) {

				// total of elapsed times for each test
				long timeTotal = 0;

				// print method's name
				System.out.println(m.getName());


				for (int ti = 0; ti < TESTS; ti++) {

					// clone the unsorted vector to use as the input
					MyVector testVec = unsorted.clone();

					// get time before calling sort method
					startTime = System.currentTimeMillis();

					// call sort method
					m.invoke(null, testVec);

					// get time after calling sort method
					endTime = System.currentTimeMillis();

					// display if method correctly sorted the vector
					if (testVec.equals(correct))
						System.out.print("   ✔ \t");
					else
						System.out.print("   ✖ \t");

					// add to total time
					timeTotal += endTime - startTime;

					// print elapsed time
					System.out.print((endTime - startTime) + "ms\t");

					// print out test indicies
					for (int i : INDICIES)
						if (i < SIZE)
							System.out.print(testVec.elementAt(i) + "\t");

					// print to next line
					System.out.println();
				}

				// print out average time for this method
				System.out.println("   avg: " + (timeTotal / TESTS) + "ms");

			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
