package lab;

import java.util.Random;
import java.util.Scanner;

import collection.MySearch;
import collection.MySort;
import collection.MyVector;

/**
 *
 * @author jacob
 */
public class Lab3 {

	public static void test() {

		MyVector numVec = new MyVector();

		// populate numVec with 900 random numbers between 100 and 999

		Random rand = new Random(System.nanoTime());
		numVec.ensureCapacity(900);
		while (numVec.size() < 900)
			numVec.append(rand.nextInt(899) + 100);

		// bubble sort
		MySort.bubbleSort(numVec);
		System.out.println("Bubble Sort:");
		System.out.println(numVec);
		System.out.println();

		Scanner r = new Scanner(System.in);

		// input number
		System.out.print("Enter number to search for: ");
		int x = r.nextInt();

		// linear search for number
		int i = MySearch.linearSearchSorted(numVec, x);
		if (i == -1)
			System.out.println("Linear Search did not find " + x);
		else
			System.out.println("Linear Search found " + x + " at " + i);


		// remove numbers between 50 and 550
		numVec.removeRange(50, 550);

		// reverse vector
		numVec.reverse();

		// selection sort vector
		MySort.selectionSort(numVec);
		System.out.println("Selection Sort:");
		System.out.println(numVec);
		System.out.println();

		// input number
		System.out.print("Enter number to search for: ");
		x = r.nextInt();

		// binary search for number
		i = MySearch.binarySeach(numVec, x);
		if (i == -1)
			System.out.println("Binary Search did not find " + x);
		else
			System.out.println("Binary Search found " + x + " at " + i);

		// close the scanner
		r.close();
	}
}
