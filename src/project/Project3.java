package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Scanner;

import collection.MyStack;
import collection.MyQueue;
import collection.MyVector;

/**
 * Project 3 test class
 * @author jacob
 */
public class Project3 {

	/**
	 * Main Program
	 */
	public static void test() {

		// try/catch for opening the file
		try {

			// load input file
			Scanner input = new Scanner(new File("COSC241_P3_Input.txt"));

			// create output file
			PrintWriter outputFile = new PrintWriter("COSC241_P3_Output_jrcoughenour0.txt", "UTF-8");

			// for each line in input file
			while(input.hasNext()) {

				// trim input line
				String infix = input.nextLine().trim();

				// skip blank lines
				if (infix.length() == 0)
					continue;


				// try parsing input

				MyQueue parsedInfix;

				try {
					parsedInfix = parseInput(infix);

				} catch (ParseException e) {

					// print out error info

					outputFile.print("\t" + infix + "\n\t");
					for (int i = e.getErrorOffset(); i > 0; i--)
						outputFile.print(" ");
					outputFile.println("^");
					outputFile.println("** " + e.getMessage() + " **");

					outputFile.println();
					outputFile.println();
					continue;
				}

				// print out elements of parsed Infix

				outputFile.print("Original Infix: \t");

				Iterator<Object> pItr = parsedInfix.iterator();
				while (pItr.hasNext()) {
					Object e = pItr.next();
					if (e instanceof Double) {
						Double v = (Double) e;
						outputFile.print(v % 1 == 0 ? v.longValue() : v);
					} else
						outputFile.print(e);
					outputFile.print(pItr.hasNext() ? ' ' : '\n');
				}

				// convert infix to postfix
				MyQueue postfix = convertToPostfix(parsedInfix);


				// print postfix

				outputFile.print("Corresponding Postfix: \t");

				Iterator<Object> cItr = postfix.iterator();
				while (cItr.hasNext()) {
					Object e = cItr.next();
					if (e instanceof Double) {
						Double v = (Double) e;
						outputFile.print(v % 1 == 0 ? v.longValue() : v);
					} else
						outputFile.print(e);
					outputFile.print(cItr.hasNext() ? ' ' : '\n');
				}

				// calculate postfix

				try {
					Object result = calcPostfix(postfix);

					// print postfix result

					outputFile.println("Evaluation Result: \t= " + result);

				} catch (IllegalArgumentException e) {
					outputFile.println("** " + e.getMessage() + " **");
				}

				outputFile.println();
				outputFile.println();
			}

			input.close();
			outputFile.close();

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
	}

	/**
	 * Parses the infix input expression into a queue where are converted
	 * to the proper data type and whitespace is removed.
	 *
	 * @param input input infix to parse
	 * @exception ParseException with message and position of invalid character
	 * @return queue of numbers and operators
	 */
	private static MyQueue parseInput(String input) throws ParseException {

		MyQueue result = new MyQueue();

		// buffer for numbers
		String numbuffer = "";
		// number in buffer is a double
		boolean isDouble = false;

		// current parenthesis depth
		int pdepth = 0;

		for (int i = 0; i < input.length(); i++) {

			char c = input.charAt(i);

			// c is part of a number
			if (Character.isDigit(c)) {
				numbuffer += c;

			// scientific notation
			} else if (c == 'e' || c == 'E') {

				// buffer is empty or already has an E
				if (numbuffer.length() == 0 || numbuffer.indexOf('E') > -1)
					throw new ParseException("invalid scientific notation", i);

				numbuffer += 'E';
				isDouble = true;

			// handle negative exponent for scientific notation
			} else if (isDouble && numbuffer.charAt(numbuffer.length() - 1) == 'E') {

				// the only valid non-digit char after E is '-'
				if (c != '-')
					throw new ParseException("invalid scientific notation", i);

				numbuffer += '-';

			// handle non-digits afer negative exponent for scientific notation
			} else if (isDouble && numbuffer.charAt(numbuffer.length() - 1) == '-') {
				throw new ParseException("invalid scientific notation", i);

			// handle float
			} else if (c == '.') {

				if (isDouble)
					throw new ParseException("multiple decimal points", i);

				numbuffer += '.';
				isDouble = true;

			// operator or whitespace
			} else {

				// number buffer isn't empty
				if (numbuffer.length() > 0) {

					Object last = result.last();
					if (last != null) {

						// handle parenthesis multiplication
						if (last.equals(')'))
							result.insertBack('*');

						else if (!(last instanceof Character))
							throw new ParseException("Missing operator between values", i - numbuffer.length() - 1);
					}

					// push the number
					result.insertBack(isDouble ? (Object) Double.parseDouble(numbuffer) : (Object) Long.parseLong(numbuffer));
					// clear buffer
					numbuffer = "";
					isDouble = false;

				}

				// parenthesis

				if (c == '(') {
					pdepth++;

					// handle parenthesis multiplication
					Object last = result.last();
					if (last != null && !(last instanceof Character))
						result.insertBack('*');

					result.insertBack('(');

				} else if (c == ')') {
					pdepth--;
					if (pdepth < 0)
						throw new ParseException("Invalid parenthesis", i);
					result.insertBack(')');

				// not whitespace
				} else if (!Character.isWhitespace(c)) {

					// invalid operator
					if (precedence(c) == -1)
						throw new ParseException("Invalid operator", i);

					// double operator or missing first value
					Object last = result.last();
					if (last == null || (last instanceof Character && (last.equals('(') || precedence((Character) last) > -1))) {

						if (c == '-' && i + 1 < input.length()) {

							int j = i + 1;
							while (j < input.length() && Character.isWhitespace(input.charAt(j)))
								j++;

							char next = input.charAt(j);

							if (next == '(') {
								result.insertBack(-1);

							} else if (Character.isDigit(next)) {
								numbuffer = "-";
								i = j - 1;

							} else
								throw new ParseException("Invalid operator missing first value", i);

						} else
							throw new ParseException("Invalid operator missing first value", i);
					} else

						result.insertBack(c);
				}
			}
		}

		// number of '(' doesn't match number of ')'
		if (pdepth > 1)
			throw new ParseException(pdepth + " missing parentheses", input.length());

		if (pdepth == 1)
			throw new ParseException(pdepth + " missing parenthesis", input.length());

		// number buffer isn't empty
		if (numbuffer.length() > 0) {

			// handle parenthesis multiplication
			Object last = result.last();
			if (last != null) {
				if (last.equals(')'))
					result.insertBack('*');
				else if (!(last instanceof Character))
					throw new ParseException("Missing operator between values", input.length() - numbuffer.length() - 1);
			}

			// push the number
			result.insertBack(isDouble ? (Object) Double.parseDouble(numbuffer) : (Object) Long.parseLong(numbuffer));

		} else {

			// trailing operator error
			Object last = result.last();
			if (last != null && (last instanceof Character && precedence((Character) last) > -1))
				throw new ParseException("Invalid operator at the end of expression", input.length()-1);
		}

		return result;
	}

	/**
	 * Converts infix expression into a postfix expression.
	 *
	 * @param exp infix expression queue
	 * @return queue of expression in postfix form
	 */
	private static MyQueue convertToPostfix(MyQueue exp) {

		MyQueue result = new MyQueue();
		MyStack<Character> ops = new MyStack<Character>();

		// for each object in expression
		for (Object c : exp) {

			// c is an operator
			if (c instanceof Character) {

				char ch = (char) c;

				// parenthesis
				if (ch == '(')
					ops.push('(');
				else if (ch == ')') {
					while (ops.size() > 0 && ops.top() != '(')
						result.insertBack(ops.pop());
					ops.pop();

				// operator
				} else {
					int p = precedence((Character) c);
					if (p > -1) {
						while (ops.size() > 0 && p <= precedence((char) ops.top()))
							result.insertBack(ops.pop());
						ops.push(ch);
					}
				}

			// c is a number
			} else
				result.insertBack(c);
		}

		while (ops.size() > 0)
			result.insertBack(ops.pop());

		return result;
	}

	/**
	 * Calculates the postfix expression to get the result.
	 *
	 * @param postfix postfix expression queue
	 * @return Double or Long result
	 */
	private static Object calcPostfix(MyQueue postfix) {

		MyStack stack = new MyStack();

		for (Object c : postfix) {

			// c is operator
			if (c instanceof Character) {

				if (stack.size() < 2)
					throw new IllegalArgumentException("Too many operators");

				stack.push(calc(stack.pop(), stack.pop(), (char) c));

			// c is a number
			} else
				stack.push(c);
		}

		return stack.top();
	}

	/**
	 * Calculates two numbers using the given operation.
	 *
	 * @param b second number
	 * @param a first number
	 * @param c operation character (+-*%^/)
	 * @return [a] [c] [b]
	 * @exception IllegalArgumentException if invalid operator or divide by zero
	 */
	private static Object calc(Object b, Object a, char c) throws IllegalArgumentException {

		// both values are longs
		if ((a instanceof Long || b instanceof Integer) && (b instanceof Long || b instanceof Integer)) {
			Long al = a instanceof Long ? (Long) a : ((Integer) a).longValue();
			Long bl = b instanceof Long ? (Long) b : ((Integer) b).longValue();

			if (c == '+') return al + bl;
			if (c == '-') return al - bl;
			if (c == '*') return al * bl;
			if (c == '%') return al % bl;
			if (c == '^') return Math.pow(al, bl);
			if (c == '/') {
				if (bl == 0L)
					throw new IllegalArgumentException("Divide by zero");
				return al / bl;
			}

			throw new IllegalArgumentException("Invalid operator " + c);
		}

		// one or both are doubles

		Double ad = a instanceof Double ? (Double) a
				: (a instanceof Long ? ((Long) a).doubleValue() : ((Integer) a).doubleValue());
		Double bd = b instanceof Double ? (Double) b
				: (b instanceof Long ? ((Long) b).doubleValue() : ((Integer) b).doubleValue());

		if (c == '+') return ad + bd;
		if (c == '-') return ad - bd;
		if (c == '*') return ad * bd;
		if (c == '%') return ad % bd;
		if (c == '^') return Math.pow(ad, bd);
		if (c == '/') {
			if (bd == 0d)
				throw new IllegalArgumentException("Divide by zero");
			return ad / bd;
		}
		throw new IllegalArgumentException("Invalid operator " + c);
	}

	/**
	 * @param c operator
	 * @return int indicating the precedence of the given operator
	 */
	private static int precedence(char c) {
		if (c == '+' || c == '-')
			return 1;
		if (c == '*' || c == '/' || c == '%')
			return 2;
		if (c == '^')
			return 3;
		return -1;
	}

}
