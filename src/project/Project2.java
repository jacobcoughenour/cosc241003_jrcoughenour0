package project;

import java.io.File;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.Scanner;

/**
 * Project 2 test class
 *
 * @author jacob
 */
public class Project2 {

	/**
	 * Numpad key for converting numbers to letters
	 */
	private static final String[] key = { "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };

	/**
	 * Main Program
	 */
	public static void test() {

		// try/catch for opening the file
		try {

			System.out.print("Loading words from file...");

			// get time before loading the file
			long startTime = System.currentTimeMillis();

			// load word list file into a scanner
			Scanner wordsScanner = new Scanner(new File("COSC241_P2_EnglishWordList.txt"));

			// lists of words indexed by their length
			ArrayList<ArrayList<String>> words = new ArrayList<ArrayList<String>>(7);

			// initialize arraylists
			for (int i = 0; i < 7; i++)
				words.add(new ArrayList());

			// load words from txt file into words list
			while(wordsScanner.hasNextLine()) {
				// get next word
				String word = wordsScanner.nextLine();

				// add word to a list based on it's length
				if (word.length() < 8 && word.length() > 0)
					words.get(word.length() - 1).add(word);
			}

			// close word list scanner
			wordsScanner.close();

			// get time after loading the file
			long endTime = System.currentTimeMillis();

			// print out elapsed time for loading the words from file
			System.out.print(" completed in " + (endTime - startTime) + "ms");
			System.out.println();
			System.out.println();

			// user input scanner
			Scanner inputScanner = new Scanner(System.in);

			while (true) {
				System.out.println("Enter a number (within 7 digits without 0 or 1)");

				// get user input
				String num = inputScanner.nextLine();

				// if user inputs exit, break loop
				if (num.equals("exit"))
					break;

				// remove non-digits chars
				num = num.replaceAll("\\D", "");

				// skip search if input does not meet requirements
				if (num.length() > 7 || num.length() == 0 || num.indexOf('0') >= 0 || num.indexOf('1') >= 0) {
					System.out.println("invalid input");
					continue;
				}

				// list of matching words
				SortedSet<String> result;

				// get time before we start search
				startTime = System.currentTimeMillis();

				// do the search
				result = getWordsForNum(words, num);

				// get time after search
				endTime = System.currentTimeMillis();

				// print results and elapsed time
				System.out.println(result.size() + " result" + (result.size() == 1 ? "" : "s") + " found in " + (endTime - startTime) + "ms");
				System.out.println(result);
				System.out.println();
			}

			// close user input scanner
			inputScanner.close();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * Gets all of the words and combinations of words that can be spelled with the
	 * given number
	 * @param words dictionary of words to search
	 * @param num   number to match
	 * @return all of the words and combinations of words that can be spelled with
	 *         the given number
	 */
	public static SortedSet<String> getWordsForNum(ArrayList<ArrayList<String>> words, String num) {

		// find single word matches
		SortedSet<String> found = getMatching(words.get(num.length() - 1), num);

		// find sub-words
		for (int i = 1; i < num.length(); i++) {

			SortedSet<String> firstWords = getWordsForNum(words, num.substring(0, i));
			SortedSet<String> secondWords = getWordsForNum(words, num.substring(i));

			// combine sub-words and add to found
			for (String firstWord : firstWords)
				for (String secondWord : secondWords)
					found.add(firstWord + '-' + secondWord);

		}

		return found;
	}

	/**
	 * Gets all the words that match the number using isMatch
	 * @param words list of words to search
	 * @param num number to compare with
	 * @return all the words that match the number using isMatch
	 */
	public static SortedSet<String> getMatching(ArrayList<String> words, String num) {

		// the words we found that match num
		SortedSet<String> found = new TreeSet<>();

		// add first char matches
		for (String word : words)
			if (isMatch(word, num))
				found.add(word);

		return found;
	}

	/**
	 * Checks if the word can be spelled with the given number
	 * @param word word to test
	 * @param num number to test
	 * @return true if the word can be spelled with the number
	 */
	public static Boolean isMatch(String word, String num) {
		// if (word.length() != num.length())
		// 	return false;

		for (int i = 0; i < word.length(); i++)
			if (key[num.charAt(i) - 50].indexOf(word.charAt(i)) == -1)
				return false;

		return true;
	}

}
