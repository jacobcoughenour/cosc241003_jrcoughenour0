package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.Scanner;

import collection.MyStack;
import collection.MyBinaryTree.Order;
import collection.MyDeque;
import collection.MyExpressionTree;

/**
 * Project 4 test class
 *
 * @author jacob
 */
public class Project4 {

	/**
	 * Main program
	 */
	public static void test() {

		// try/catch for opening the file
		try {

			// load input file
			Scanner input = new Scanner(new File("COSC241_P4_Input.txt"));

			// create output file
			PrintWriter outputFile = new PrintWriter("COSC241_P4_Output_jrcoughenour0.txt", "UTF-8");

			// for each line in input file
			while (input.hasNextLine()) {

				// trim input line
				String infix = input.nextLine().trim();

				// skip blank lines
				if (infix.length() == 0)
					continue;

				// try parsing input

				MyDeque parsedInfix;

				// outputFile.print("Original Infix:");
				outputFile.print("Original Infix: \t");

				try {
					parsedInfix = parseInput(infix);

				} catch (ParseException e) {

					// print out error info

					outputFile.println("\t" + infix);
					// outputFile.print("\t" + infix + "\n\t");
					// for (int i = e.getErrorOffset(); i > 0; i--)
					// outputFile.print(" ");
					// outputFile.println("^");
					outputFile.println("** " + e.getMessage() + " **");

					outputFile.println();
					outputFile.println();
					continue;
				}

				Iterator<Object> pItr = parsedInfix.iterator();
				while (pItr.hasNext()) {
					outputFile.print(pItr.next());
					outputFile.print(pItr.hasNext() ? ' ' : '\n');
				}

				// convert infix to postfix
				MyStack postfix = convertToPostfix(parsedInfix);

				// create the expression tree from the postfix stack
				MyExpressionTree<Object> tree = new MyExpressionTree<Object>(postfix.iterator());

				// print different traversals

				outputFile.print("Preorder traversal:\t");
				outputFile.println(tree.toString(Order.Preorder, " "));

				outputFile.print("Inorder traversal:\t");
				outputFile.println(tree.toString(Order.Inorder, " "));

				outputFile.print("Postorder traversal:\t");
				outputFile.println(tree.toString(Order.Postorder, " "));

				// print result

				outputFile.print("Evaluation Result:\t= " + tree.evaluate().toString());
				outputFile.println();

				outputFile.println();
				outputFile.println();
			}

			input.close();
			outputFile.close();

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

	}

	/**
	 * Parse input expression and put all the tokens into a deque
	 *
	 * @param input input expression
	 * @return deque with each token as a char, double, or long
	 * @throws ParseException If the expression is invalid
	 */
	private static MyDeque parseInput(String input) throws ParseException {

		// add some extra whitespace to operators to separate them from values

		String paddedinput = "";
		char lastc = '_';

		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c == '(' || c == ')'
					|| (precedence(c) > -1 && !((c == '-' || c == '+') && (lastc == 'e' || lastc == 'E'))))
				paddedinput += " " + c + " ";
			else
				paddedinput += c;
			lastc = c;
		}

		MyDeque result = new MyDeque();
		Scanner s = new Scanner(paddedinput);

		// number of '(' - number of ')'
		int pdepth = 0;

		// number of values - number of operators
		int numdepth = 0;

		while (s.hasNext()) {

			// next is a value
			if (s.hasNextLong()) {
				result.append(s.nextLong());
				numdepth++;
			} else if (s.hasNextDouble()) {
				result.append(s.nextDouble());
				numdepth++;
			}

			// next is an operator, +- prefix, or invalid
			else {

				String n = s.next();

				if (n.length() != 1)
					throw new ParseException("Invalid operator", s.match().end());

				char ch = n.charAt(0);

				// negative before parentheses (Ex: "-(22)")
				if (numdepth == 0 && ch == '-' && s.hasNext("\\(")) {
					// just inject "-1 *" before it
					result.append(-1);
					result.append('*');
				}
				// negative or positive value (Ex: "+40 + -23")
				else if (numdepth == 0 && (ch == '-' || ch == '+') && (s.hasNextLong() || s.hasNextDouble())) {
					if (s.hasNextLong())
						result.append(ch == '-' ? -s.nextLong() : s.nextLong());
					else if (s.hasNextDouble())
						result.append(ch == '-' ? -s.nextDouble() : s.nextDouble());

					numdepth++;
				}

				// operator or invalid
				else {

					if (ch == '(')
						pdepth++;
					else if (ch == ')')
						pdepth--;
					else {

						// invalid operator
						if (precedence(ch) == -1)
							throw new ParseException("Invalid operator", s.match().end());
						numdepth--;

						// too many operators
						if (numdepth < 0)
							throw new ParseException("Invalid operators", input.length());
					}

					// add valid operator
					result.append(ch);
				}

				// too many closing parentheses
				if (pdepth < 0)
					throw new ParseException("Invalid parentheses", s.match().end());
			}

		}

		// number of '(' doesn't match number of ')'
		if (pdepth > 1)
			throw new ParseException(pdepth + " missing parentheses", input.length());
		if (pdepth == 1)
			throw new ParseException(pdepth + " missing parenthesis", input.length());

		// invalid trailing operator
		Object last = result.last();
		if (last != null && (last instanceof Character && precedence((Character) last) > -1))
			throw new ParseException("Invalid operator at the end of expression", input.length() - 1);

		// numdepth should be 1
		else if (numdepth != 1)
			throw new ParseException("Invalid number of operators", input.length());

		// close the scanner
		s.close();

		return result;
	}

	/**
	 * Converts infix expression into a postfix expression.
	 *
	 * @param exp infix expression queue
	 * @return stack of expression in postfix form
	 */
	private static MyStack convertToPostfix(MyDeque exp) {

		MyStack result = new MyStack();
		MyStack<Character> ops = new MyStack<Character>();

		// for each object in expression
		for (Object c : exp) {

			// c is an operator
			if (c instanceof Character) {

				char ch = (char) c;

				// parenthesis
				if (ch == '(')
					ops.push('(');
				else if (ch == ')') {
					while (ops.size() > 0 && ops.top() != '(')
						result.push(ops.pop());
					ops.pop();

					// operator
				} else {
					int p = precedence((Character) c);
					if (p > -1) {
						while (ops.size() > 0 && p <= precedence((char) ops.top()))
							result.push(ops.pop());
						ops.push(ch);
					}
				}

				// c is a number
			} else
				result.push(c);
		}

		// push remaining operators onto result
		while (ops.size() > 0)
			result.push(ops.pop());

		return result;
	}

	/**
	 * @param c operator
	 * @return int indicating the precedence of the given operator
	 */
	private static int precedence(char c) {
		if (c == '+' || c == '-')
			return 1;
		if (c == '*' || c == '/' || c == '%')
			return 2;
		if (c == '^')
			return 3;
		return -1;
	}
}
