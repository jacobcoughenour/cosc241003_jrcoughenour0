package project;

import collection.MySet;

/**
 * Project 1 test class
 * @author jacob
 */
public class Project1 {

	public static void test() {

		// first 30 fibonacci numbers
		MySet fibonacciNumSet = new MySet(0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584,4181, 6765, 10946, 17711, 28657, 46368, 75025, 121393, 196418, 317811, 514229);

		// prints out 29 numbers because the duplicate 1 is removed
		System.out.println("fibonacciNumSet");
		System.out.println(fibonacciNumSet);
		System.out.println();

		// first 32 prime numbers
		MySet primeNumSet = new MySet(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131);

		System.out.println("primeNumSet");
		System.out.println(primeNumSet);
		System.out.println();

		// intersection of fibonacci and prime numbers
		System.out.println("Intersection");
		System.out.println(fibonacciNumSet.intersection(primeNumSet));
		System.out.println();

		// symmetric difference of fibonacci and prime numbers
		System.out.println("Symmetric difference");
		System.out.println(fibonacciNumSet.symmetricDifference(primeNumSet));
		System.out.println();

		// union of fibonacci and prime numbers
		System.out.println("Union");
		System.out.println(fibonacciNumSet.union(primeNumSet));
		System.out.println();

	}
}
