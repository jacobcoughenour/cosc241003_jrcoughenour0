package collection;

/**
 * Deque List
 * 
 * @author jacob
 */
public class MyDeque<T> extends DLList<T> {

	/**
	 * MyDeque Constructor Appends given elements
	 */
	public MyDeque(Object... elements) {
		super(elements);
	}

	/**
	 * Removes the first element in the deque
	 * 
	 * @return element removed or null if empty
	 */
	public Object popFirst() {
		if (size == 0)
			return null;

		Object e = head.data;

		head = head.next;

		if (head != null)
			head.prev = null;

		size--;

		if (size == 0)
			tail = null;

		return e;
	}

	/**
	 * Removes the last element in the deque
	 * 
	 * @return element removed or null if empty
	 */
	public Object popLast() {
		if (size == 0)
			return null;

		Object e = tail.data;

		tail = tail.prev;

		if (tail != null)
			tail.next = null;

		size--;

		if (size == 0)
			head = null;

		return e;
	}

	/**
	 * @return first element in the deque
	 */
	public Object first() {
		if (head == null)
			return null;
		return head.data;
	}

	/**
	 * @return last element in the deque
	 */
	public Object last() {
		if (tail == null)
			return null;
		return tail.data;
	}
}