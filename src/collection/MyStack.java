package collection;

import java.util.Iterator;

/**
 * MyStack class
 * @author jacob
 */
public class MyStack<T> implements MyCollection<T> {

	protected SLListNode<T> top;
	protected int size;

	/**
	 * MyStack default constructor
	 */
	public MyStack() {
		size = 0;
	}

	/**
	 * MyStack constructor
	 * @param elements elements to initialize with.
	 * Last element is the first one out.
	 */
	public MyStack(Object... elements) {
		size = 0;

		// add all the elements
		for (Object e : elements)
			this.push((T) e);
	}

	/**
	 * push the given element into the stack
	 * @param element element to push
	 */
	public void push(T element) {

		// ignore null values
		if (element == null)
			return;

		// add element to top and have it reference the previous top node
		top = new SLListNode(element, top);

		size++;
	}

	/**
	 * Removes the element at the top of the stack and returns it
	 * @return the element that was removed
	 */
	public T pop() {
		if (size == 0)
			return null;

		Object element = top.data;
		top = top.next;

		size--;
		return (T) element;
	}

	/**
	 * @return Element at the top of the stack
	 */
	public T top() {
		if (top == null)
			return null;

		return (T) top.data;
	}

	/**
	 * @return The current size of the MyStack
	 */
	public int size() {
		return size;
	}

	/**
	 * @return String representation of the current elements in this stack
	 */
	@Override
	public String toString() {
		String s = "[";
		SLListNode node = top;

		while (node != null) {
			s += node.data.toString();
			if (node.next != null)
				s += ", ";
			node = node.next;
		}

		return s + "]";
	}

	public Iterator<T> iterator() {
		return new StackIterator();
	}

	private class StackIterator implements Iterator<T> {

		SLListNode cur;

		public StackIterator() {
			cur = top;
		}

		public boolean hasNext() {
			return cur != null;
		}

		public T next() {
			Object data = cur.data;
			cur = cur.next;
			return (T) data;
		}

	}
}