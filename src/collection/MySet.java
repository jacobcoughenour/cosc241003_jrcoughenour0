package collection;

/**
 * MySet
 * @author jacob
 */
public class MySet extends MyVector {

	/**
	 * MySet default contructor
	 * Creates an empty MySet
	 */
	public MySet() {
		super();
	}

	/**
	 * MySet constructor
	 * Creates MySet and adds the given elements
	 * @param elements elements to add
	 */
	public MySet(Object... elements) {

		// Create MyVector with initial capacity
		super(elements.length);

		for (Object e : elements)
			if (e != null)
				this.add(e);
	}

	protected MySet(int initialCapacity) {
		super(initialCapacity);
	}

	/**
	 * Number of elements in this set
	 * @return
	 */
	public int cardinality() {
		return this.size;
	}

	/**
	 * The compliment of this set and the given set
	 * @param b
	 * @return this set - b
	 */
	public MySet complement(MySet b) {
		MySet compSet = new MySet();

		for (int i = 0; i < b.size; i++)
			if (!this.contains(b.data[i]))
				compSet.add(b.data[i]);

		return compSet;
	}

	/**
	 * Add given element to this set, but exclude duplicates
	 * @param element element to add
	 */
	public void add(Object element) {
		if (!this.contains(element))
			super.append(element);
	}

	/**
	 * Use {@link MySet#add add} instead
	 */
	@Override
	public void append(Object element) {
		this.add(element);
	}

	/**
	 * @param b
	 * @return new MySet contianing the matching elements from this set and the given set
	 */
	public MySet intersection(MySet b) {
		MySet interSet = new MySet();

		for (int i = 0; i < b.size; i++)
			if (this.contains(b.data[i]))
				interSet.add(b.data[i]);

		return interSet;
	}

	/**
	 * @param b
	 * @return whether this is a subset of set b
	 */
	public boolean subsetOf(MySet b) {

		for (int i = 0; i < size; i++)
			if (!b.contains(data[i]))
				return false;

		return true;
	}

	/**
	 * @param b
	 * @return new MySet of the symmetric difference between this set and the given set
	 */
	public MySet symmetricDifference(MySet b) {

		MySet diffSet = new MySet();

		for (int i = 0; i < b.size; i++)
			if (!this.contains(b.data[i]))
				diffSet.add(b.data[i]);

		for (int i = 0; i < size; i++)
			if (!b.contains(data[i]))
				diffSet.add(data[i]);

		return diffSet;
	}

	/**
	 * @param b
	 * @return new MySet contain all the unique elements from both this set and the given set
	 */
	public MySet union(MySet b) {

		MySet unionSet = clone();

		for (int i = 0; i < b.size; i++)
			unionSet.add(b.data[i]);

		return unionSet;
	}

	/**
	 * Creates a clone of this set
	 * @return the cloned set
	 */
	@Override
	public MySet clone() {

		MySet cloned = new MySet(size);
		cloned.size = size;

		for (int i = 0; i < size; i++)
			cloned.data[i] = data[i];

		return cloned;
	}

	/**
	 * @return String representation of the current elements in this set
	 */
	@Override
	public String toString() {

		String s = "MySet " + size + " [";

		if (size == 0)
			return s + "]";

		s += data[0].toString();

		for (int i = 1; i < size; i++)
			s += "," + data[i].toString();

		return s + "]";
	}
}
