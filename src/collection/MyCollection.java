package collection;

import java.util.Iterator;

/**
 * Interface for collections in collection package.
 *
 * @extends {@link Iterable}
 */
public interface MyCollection<T> extends Iterable<T> {

	Iterator<T> iterator();
}