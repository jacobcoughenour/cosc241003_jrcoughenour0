package collection;

/**
 * MySearch class
 * @author jacob
 */
public class MySearch {

	private MySearch() {}

	/**
	 * Binary Search MyVector for given target element
	 * @param v sorted vector
	 * @param target target to search for
	 * @return the first index where the element exists in the vector, or -1 if not present
	 */
	public static int binarySeach(MyVector v, Comparable target) {

		int first = 0;
		int last = v.size() - 1;
		int middle;

		while(first <= last) {

			// get middle index
			middle = (first + last) / 2;

			// compare target to element at middle index
			int c = target.compareTo(v.elementAt(middle));

			// go left
			if (c < 0)
				last = middle - 1;

			// go right
			else if (c > 0)
				first = middle + 1;

			// found it
			else
				return middle;
		}

		// didn't find it
		return -1;
	}

	/**
	 * Linear Search MyVector for given target element
	 * @param v sorted vector
	 * @param target target to search for
	 * @return the first index where the element exists in the vector, or -1 if not
	 * present
	 */
	public static int linearSearchSorted(MyVector v, Comparable target) {

		// for each element in vector
		for (int i = 0; i < v.size(); i++) {

			// compare element to given target
			int c = target.compareTo(v.elementAt(i));

			// element is equal to target
			if (c == 0)
				return i;

			// element has a greater value than target
			// therefore the target is not present in rhe vector
			if (c < 0)
				return -1;
		}

		// target not found
		return -1;
	}

}
