package collection;

/**
 * Node for Singly Link Lists
 * @author jacob
 */
public class SLListNode<T> {

	public T data;
	public SLListNode next;

	/**
	 * SLListNode constructor
	 * @param data data for this node
	 */
	public SLListNode(T data) {
		this.data = data;
	}

	/**
	 * SLListNode constructor with reference to next node
	 * @param data data for this node
	 * @param next reference to next node
	 */
	public SLListNode(T data, SLListNode next) {
		this.data = data;
		this.next = next;
	}

}