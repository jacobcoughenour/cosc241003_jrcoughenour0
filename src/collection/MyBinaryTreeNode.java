package collection;

/**
 * Node for Binary Tree
 *
 * @author jacob
 */
public class MyBinaryTreeNode<T> {

	public T data;
	public MyBinaryTreeNode<T> left;
	public MyBinaryTreeNode<T> right;

	/**
	 * MyBinaryTreeNode constructor
	 *
	 * @param data data for this node
	 */
	public MyBinaryTreeNode(T data) {
		this.data = data;
	}

	/**
	 * MyBinaryTreeNode constructor with reference to the left and right nodes
	 *
	 * @param data  data for this node
	 * @param left  reference to left node
	 * @param right reference to right node
	 */
	public MyBinaryTreeNode(T data, MyBinaryTreeNode<T> left, MyBinaryTreeNode<T> right) {
		this.data = data;
		this.left = left;
		this.right = right;
	}

}