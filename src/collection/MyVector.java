package collection;

import java.util.Iterator;

/**
 * MyVector
 * @author jacob
 */
public class MyVector<T> implements MyCollection<T>, Cloneable {

	protected Object[] data;
	protected int size;

	protected static final int INITIAL_CAPACITY = 10;

	/**
	 * MyVector default constructor
	 */
	public MyVector() {
		this(INITIAL_CAPACITY);
	}

	/**
	 * MyVector constructor
	 * @param elements elements to initialize with
	 */
	public MyVector(Object... elements) {

		// create MyVector with enough capacity
		this(elements.length);

		// add all the elements to it
		for (Object e : elements)
			// ignore null values
			if (e != null)
				this.append(e);
	}

	/**
	 * MyVector constructor
	 * Initializes data with given capacity.
	 *
	 * Make sure that you are using int and not Integer.
	 * @param initialCapacity
	 */
	public MyVector(int initialCapacity) {

		// initialize with array with initial capacity
		data = new Object[initialCapacity];
		size = 0;
	}

	/**
	 * append the given element to the end of the vector
	 *
	 * @param element element to append
	 */
	public void append(Object element) {

		// if we have used up all of data array
		if (data.length == size)
			// expand array
			expand();

		// add element to data array
		data[size++] = element;
	}

	/**
	 * Removes all elements from vector
	 */
	public void clear() {

		// replace data with new array
		data = new Object[INITIAL_CAPACITY];
		size = 0;
	}

	/**
	 * returns whether the vector contains the given element
	 *
	 * @param element element to search for
	 * @return true if vector contains given element
	 */
	public boolean contains(Object element) {
		// return whether we can find the index of the element
		return indexOf(element) != -1;
	}

	/**
	 * Gets the element at the given index
	 * @param index index of the element
	 * @return element at the given index, null if index out of bounds
	 */
	public T elementAt(int index) {
		if (index < 0 || index >= size)
			return null;

		return (T) data[index];
	}

	/**
	 * @return the first element in the vector
	 */
	public T first() {
		return elementAt(0);
	}

	/**
	 * @return element at the end of the vector
	 */
	public T last() {
		return elementAt(size-1);
	}

	/**
	 * @return a new vector containing the elements between fromIndex, inclusive and
	 * the end of the vector.
	 * @param fromIndex start of the range
	 */
	public MyVector slice(int fromIndex) {
		return this.slice(fromIndex, this.size);
	}

	/**
	 * @return a new vector containing the elements between fromIndex, inclusive and toIndex, exclusive.
	 * @param fromIndex start of the range
	 * @param toIndex end of the range (exclusive)
	 */
	public MyVector slice(int fromIndex, int toIndex) {

		fromIndex = Math.max(0, fromIndex);
		toIndex = Math.min(this.size, toIndex);

		if (toIndex - fromIndex <= 0)
			return new MyVector();

		MyVector vec = new MyVector(toIndex - fromIndex);

		while (fromIndex < toIndex) {
			vec.append(data[fromIndex]);
			fromIndex++;
		}

		return vec;
	}

	/**
	 * returns the first index of the given element
	 *
	 * @param element element to search for
	 * @return the first index where the element exists in the vector, or -1 if
	 * not present
	 */
	public int indexOf(Object element) {

		// for each element in vector
		for (int i = 0; i < size; i++)
			// found a match
			if (data[i].equals(element))
				// return match index
				return i;

		// element not found
		return -1;
	}

	/**
	 * inserts element into the vector at the given index
	 * @param index index to insert the given element at
	 * @param element element to insert at the given index
	 * @return true if inserted successfully
	 */
	public boolean insertAt(int index, T element) {
		if (index < 0 || index > size)
			return false;

		// if inserting at the end, just append
		if (index == size) {
			append(element);
			return true;
		}

		// expand if data is full
		if (size == data.length)
			expand();

		// shift values right by 1, from 'index' to end
		for (int i = size; i > index; i--)
			data[i] = data[i - 1];

		// add the element
		data[index] = element;
		size++;

		return true;
	}

	/**
	 * Check whether the vector is empty
	 * @return true if size is 0
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Remove element at given index
	 * @param index index of element to remove
	 * @return removed element (null if index is out of range)
	 */
	public T removeAt(int index) {
		if (index < 0 || index >= size)
			return null;

		T element = data[index];

		// shift values backwards between 'index' and 'size'
		for (int i = index; i < size - 1; i++)
			data[i] = data[i + 1];

		// remove the last element the element
		size--;
		data[size] = null;

		return element;
	}

	/**
	 * Removes given element from MyVector
	 * @param element element to remove
	 * @return true if the element was removed
	 */
	public boolean remove(Object element) {
		return removeAt(indexOf(element)) != null;
	}

	/**
	 * Replace the element at the given index with the given element
	 * @param index index of element to replace
	 * @param element element to insert at index
	 */
	public void replace(int index, T element) {
		if (index < 0 || index >= size)
			return;

		data[index] = element;
	}

	/**
	 * Swap two elements at given indicies
	 * @param a first index
	 * @param b second index
	 */
	public void swap(int a, int b) {
		if (a < 0 || a >= size || b < 0 || b >= size)
			return;

		T temp = data[a];
		data[a] = data[b];
		data[b] = temp;
	}

	/**
	 * @return The current size of the MyVector
	 */
	public int size() {
		return size;
	}

	/**
	 * Make sure the vector gets at least the given capacity
	 * @param minCapacity minimum capacity to ensure
	 */
	public void ensureCapacity(int minCapacity) {

		// we already have that capacity
		if (minCapacity <= data.length)
			return;

		Object[] temp = new Object[minCapacity];

		for (int i = 0; i < data.length; i++)
			temp[i] = data[i];

		data = temp;
	}

	/**
	 * Creates a clone of this vector
	 * @return the cloned vector
	 */
	public MyVector clone() {

		MyVector<T> cloned = new MyVector<T>(size);

		for (int i = 0; i < size; i++)
			cloned.data[i] = data[i];

		cloned.size = size;

		return cloned;
	}

	/**
	 * Removes all of the elements whose index is between fromIndex, inclusive and toIndex, exclusive
	 * @param fromIndex start of the range
	 * @param toIndex end of the range (exclusive)
	 */
	public void removeRange(int fromIndex, int toIndex) {
		if (fromIndex >= toIndex)
			return;
		if (fromIndex < 0)
			fromIndex = 0;
		if (toIndex >= size)
			toIndex = size;

		int dist = toIndex - fromIndex;

		for (int i = fromIndex; i < toIndex && i + dist < size; i++)
			data[i] = data[i + dist];

		for (int i = toIndex; i < size; i++)
			data[i] = null;

		size -= dist;
	}

	/**
	 * @return String representation of the current elements in this vector
	 */
	@Override
	public String toString() {
		String s = "MyVector<> [";

		if (size == 0)
			return s + "]";

		s += data[0].toString();

		for (int i = 1; i < size; i++)
			s += "," + data[i].toString();

		return s + "]";
	}

	/**
	 * @return if object is an instance of MyVector and every element is equal()
	 */
	@Override
	public boolean equals(Object o) {

		if (o instanceof MyVector) {
			MyVector b = o;

			if (size != b.size)
				return false;

			for (int i = 0; i < size; i++)
				if (!(data[i]).equals(b.data[i]))
					return false;

			return true;
		}

		return false;
	}

	/**
	 * Reverse the order of elements in the vector
	 */
	public void reverse() {

		T temp;

		for (int i = 0; i < size/2; i++) {
			temp = data[i];
			data[i] = data[size-i-1];
			data[size-i-1] = temp;
		}

	}

	/**
	 * Appends all the elements from given vector to this vector
	 * @param vector2 vector to append
	 */
	public void merge(MyVector vector2) {

		ensureCapacity(size + vector2.size);

		for (int i = 0; i < vector2.size; i++)
			data[size + i] = vector2.data[i];

		size += vector2.size;

	}

	/**
	 * expand data array by double it's current size
	 */
	private void expand() {

		// hold onto the old array
		Object[] old = data;

		// replace with new array
		data = new Object[old.length * 2];

		// move elements over to the new array
		for (int i = 0; i < old.length; i++)
			data[i] = old[i];

	}

	public Iterator<T> iterator() {
		return new VectorIterator();
	}

	private class VectorIterator implements Iterator<T> {

		int index;

		public boolean hasNext() {
			return index != size;
		}

		public T next() {
			Object e = elementAt(index);
			index++;
			return (T) e;
		}

	}
}
