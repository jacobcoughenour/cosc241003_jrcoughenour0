package collection;

/**
 * Sorted Singly Linked List
 * @author jacob
 */
public class SortedSLList extends SLList {

	/**
	 * SortedSLList constructor
	 */
	public SortedSLList() {
		super();
	}

	/**
	 * Use {@link SortedSLList#insert insert} instead
	 */
	@Override
	public void append(Object element) {
		this.insert(element);
	}

	/**
	 * Inserts the given element at the right location to maintain order
	 * @param element element to insert
	 */
	@Override
	public void insert(Object element) {

		// list is empty
		if (size == 0) {
			head = tail = new SLListNode(element);
			size = 1;
			return;
		}

		// create a comparable for the element
		Comparable<Object> c = (Comparable<Object>) element;

		// smaller than head
		if (c.compareTo(head.data) < 0) {
			head = new SLListNode(element, head);
			size++;
			return;
		}

		// current node
		SLListNode node = head;

		// interate untill we find insertion point
		while (node.next != null && c.compareTo(node.next.data) > 0)
			node = node.next;

		// insert element bewteen last and node
		node.next = new SLListNode(element, node.next);

		size++;
	}

	/**
	 * Removes the given element from the list
	 * @param element element to remove
	 * @return true if element was removed
	 */
	@Override
	public boolean remove(Object element) {

		if (size == 0)
			return false;

		Comparable<Object> c = (Comparable<Object>) element;

		int diff = c.compareTo(head.data);

		if (diff == 0) {
			if (head.next == null)
				tail = head;

			head = head.next;

			size--;
			return true;
		}

		// current node
		SLListNode node = head;

		while (node.next != null && diff > 0) {

			diff = c.compareTo(node.next.data);

			if (diff == 0) {

				if (node.next == tail)
					tail = node;

				node.next = node.next.next;

				size--;
				return true;
			}

			node = node.next;
		}

		return false;
	}
}