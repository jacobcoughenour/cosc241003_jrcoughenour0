package collection;

/**
 * MySort class
 * @author jacob
 */
public class MySort {

	/**
	 * Bubble sort elements in given vector
	 * @param vec vector to sort
	 */
	public static void bubbleSort(MyVector vec) {

		for (int i = 0; i < vec.size(); i++)
			for (int j = 1; j < vec.size(); j++)

				// if a > b
				if (((Comparable<Object>) vec.elementAt(j - 1)).compareTo(vec.elementAt(j)) > 0)
					vec.swap(j - 1, j);
	}

	/**
	 * Selection sort given vector
	 * @param vec vector to sort
	 */
	public static void selectionSort(MyVector vec) {

		// for each selection
		for (int i = 0; i < vec.size() - 1; i++) {

			// find minimum in selection [i, vec.size-1]

			int minIndex = i;
			Object min = vec.elementAt(minIndex);

			for (int j = i + 1; j < vec.size(); j++) {

				Object e = vec.elementAt(j);

				// if element is smaller than min
				if (((Comparable<Object>) e).compareTo(min) < 0) {
					minIndex = j;
					min = e;
				}
			}

			// swap element at i with min
			vec.replace(minIndex, vec.elementAt(i));
			vec.replace(i, min);
		}
	}

	/**
	 * Merge sort given vector
	 * @param vec vector to sort
	 */
	public static void mergeSort(MyVector vec) {

		if (vec.size() < 2)
			return;

		int middle = vec.size() / 2;

		MyVector vec1 = vec.slice(0, middle);
		MyVector vec2 = vec.slice(middle);

		mergeSort(vec1);
		mergeSort(vec2);

		vec.clear();
		vec.merge(merge(vec1, vec2));

	}

	/**
	 * Merge and sort given vectors
	 * @param vec1 vector 1
	 * @param vec2 vector 2
	 * @return new merged and sorted vector
	 */
	private static MyVector merge(MyVector vec1, MyVector vec2) {

		MyVector vec = new MyVector(vec1.size() + vec2.size());

		int ai = 0;
		int bi = 0;

		Object a = vec1.elementAt(ai);
		Object b = vec2.elementAt(bi);

		while (ai < vec1.size() && bi < vec2.size()) {

			if (((Comparable<Object>) a).compareTo(b) < 0) {
				vec.append(a);
				ai++;
				a = vec1.elementAt(ai);

			} else {
				vec.append(b);
				bi++;
				b = vec2.elementAt(bi);
			}
		}

		if (ai < vec1.size())
			vec.merge(vec1.slice(ai));
		else if (bi < vec2.size())
			vec.merge(vec2.slice(bi));

		return vec;
	}

	/**
	 * Quick sort given vector
	 * @param vec vector to sort
	 */
	public static void quickSort(MyVector vec) {
		quickSort(vec, 0, vec.size() - 1);
	}

	/**
	 * Quick sort given vector between left and right indicies
	 * @param vec vector to sort
	 * @param left start of range to sort
	 * @param right end of range to sort
	 */
	private static void quickSort(MyVector vec, int left, int right) {

		// pivot to compare against
		Comparable<Object> pivot = (Comparable<Object>) vec.elementAt(((right - left) / 2) + left);

		int i = left;
		int j = right;

		while (i <= j) {

			// find first element greater than pivot
			while (pivot.compareTo(vec.elementAt(i)) > 0)
				i++;

			// find last element less than pivot
			while (pivot.compareTo(vec.elementAt(j)) < 0)
				j--;

			if (i <= j) {
				vec.swap(i, j);
				i++;
				j--;
			}
		}

		if (left < j)
			quickSort(vec, left, j);
		if (i < right)
			quickSort(vec, i, right);

	}

	/**
	 * Insertion sort given vector
	 * @param vec vector to sort
	 */
	public static void insertionSort(MyVector vec) {

		// for size-1
		for (int i = 1; i < vec.size(); i++)

			// from i to 1
			for (int j = i; j > 0; j--)

				// swap if vec[j] < vec[j-1]
				if (((Comparable<Object>) vec.elementAt(j)).compareTo(vec.elementAt(j-1)) < 0)
					vec.swap(j-1, j);
	}

	/**
	 * Shell sort given vector
	 * @param vec vector to sort
	 */
	public static void shellSort(MyVector vec) {

		// interval
		int c = vec.size() / 2;

		// while interval is > 0
		while (c > 0) {

			for (int i = c; i < vec.size(); i++) {

				int j = i;
				Object temp = vec.elementAt(i);
				Comparable<Object> e = (Comparable<Object>) temp;

				while (j >= c && e.compareTo(vec.elementAt(j - c)) < 0) {
					vec.replace(j, vec.elementAt(j - c));
					j -= c;
				}

				vec.replace(j, temp);
			}

			if (c == 2) c = 1;
			else c *= (5.0 / 11);
		}
	}
}
