package collection;

import java.util.Iterator;

/**
 * Double Linked List
 *
 * @author jacob
 */
public class DLList<T> implements MyCollection<T> {

	protected DLListNode head;
	protected DLListNode tail;
	protected int size;

	/**
	 * DLList Constructor
	 */
	public DLList() {
		head = null;
		tail = null;
		size = 0;
	}

	/**
	 * DLList constructor Appends given elements
	 */
	public DLList(Object... elements) {
		this();
		append(elements);
	}

	/**
	 * Clear all elements from list
	 */
	public void clear() {
		head = null;
		tail = null;
		size = 0;
	}

	/**
	 * @return true if list is empty
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * @return current number of elements
	 */
	public int size() {
		return size;
	}

	/**
	 * Appends given elements to the tail
	 *
	 * @param elements elements to append
	 */
	public void append(Object... elements) {
		for (Object e : elements)
			this.append(e);
	}

	/**
	 * Appends given element to the tail
	 *
	 * @param element element to append
	 */
	public void append(Object element) {
		DLListNode node = new DLListNode(element, tail);

		if (isEmpty())
			head = node;
		else
			tail.next = node;

		tail = node;
		size++;
	}

	/**
	 * Inserts the given elements at the head
	 *
	 * @param elements elements to insert
	 */
	public void insert(Object... elements) {
		for (Object e : elements)
			this.insert(e);
	}

	/**
	 * Inserts the given element at the head
	 *
	 * @param element element to insert
	 */
	public void insert(Object element) {

		DLListNode node = new DLListNode(element, null, head);

		// if empty, also set the tail
		if (size == 0) {
			tail = head = node;
		} else {
			head.prev = node;
			head = node;
		}

		size++;
	}

	/**
	 * Removes the given elements from the list
	 */
	public void remove(Object... elements) {
		for (Object e : elements)
			this.remove(e);
	}

	/**
	 * Removes the given element from the list
	 *
	 * @param element element to remove
	 * @return true if element was removed
	 */
	public boolean remove(Object element) {

		if (size == 0)
			return false;

		DLListNode node = head;

		// if head == element
		if (node.data.equals(element)) {

			if (size == 1) {
				tail = head = null;
			} else {
				head = head.next;
				head.prev = null;
			}

			size--;
			return true;
		}

		while (node.next != null) {

			if (node.next.data.equals(element)) {

				// tail == element
				if (node.next == tail)
					tail = node;
				else
					node.next.next.prev = node;

				node.next = node.next.next;

				size--;
				return true;
			}

			node = node.next;
		}

		return false;
	}

	/**
	 * @return String representation of the current elements in this list
	 */
	@Override
	public String toString() {
		String s = "[";
		DLListNode node = head;

		while (node != null) {
			s += node.data.toString();
			if (node.next != null)
				s += ", ";
			node = node.next;
		}

		return s + "]";
	}

	public Iterator<T> iterator() {
		return iterator(false);
	}

	/**
	 * Returns an iterator over elements of type T.
	 *
	 * @param reverse start at the end and go in reverse
	 * @return an Iterator.
	 */
	public Iterator<T> iterator(boolean reverse) {
		return new DLListIterator(reverse);
	}

	private class DLListIterator implements Iterator<T> {

		private boolean reverse = false;
		private DLListNode cur;

		public DLListIterator(boolean reverse) {
			if (reverse)
				cur = tail;
			else
				cur = head;
			this.reverse = reverse;
		}

		public boolean hasNext() {
			return cur != null;
		}

		public T next() {
			Object data = cur.data;
			cur = reverse ? cur.prev : cur.next;
			return (T) data;
		}

	}
}