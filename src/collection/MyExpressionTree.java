package collection;

import java.util.Iterator;

/**
 * Expression Tree
 *
 * @author jacob
 */
public class MyExpressionTree<T> extends MyBinaryTree<T> {

	/**
	 * Expression Tree constructor
	 */
	public MyExpressionTree() {
		super();
	}

	/**
	 * Expression Tree constructor.
	 *
	 * Populates the tree with an iterator in postfix order.
	 *
	 * @param iterator iterator to populate the tree with
	 */
	public MyExpressionTree(Iterator<Object> iterator) {
		if (iterator.hasNext())
			root = insertPostfix(iterator);
	}

	/**
	 * Recursively inserts a postfix expression using an iterator
	 */
	private MyBinaryTreeNode<T> insertPostfix(Iterator<Object> iterator) {
		if (!iterator.hasNext())
			return null;

		Object cur = iterator.next();

		if (cur instanceof Character) {
			MyBinaryTreeNode right = insertPostfix(iterator);
			return new MyBinaryTreeNode(cur, insertPostfix(iterator), right);
		}
		return new MyBinaryTreeNode(cur);
	}

	/**
	 * Evaluates the current expression.
	 *
	 * @return the result
	 */
	public Object evaluate() {
		return evaluate(root);
	}

	/**
	 * Recursively evaluates the expression tree
	 *
	 * @param node sub-tree root to evaluate from
	 * @return the result
	 */
	private static Object evaluate(MyBinaryTreeNode node) {
		if (node.data instanceof Character)
			return calc(evaluate(node.left), evaluate(node.right), (Character) node.data);

		return node.data;
	}

	/**
	 * Calculates two numbers using the given operation.
	 *
	 * @param a first number
	 * @param b second number
	 * @param c operation character (+-*%^/)
	 * @return [a] [c] [b]
	 * @exception IllegalArgumentException if invalid operator or divide by zero
	 */
	private static Object calc(Object a, Object b, char c) throws IllegalArgumentException {

		// both values are ints
		if ((a instanceof Long || a instanceof Integer) && (b instanceof Long || b instanceof Integer)) {
			Long al = a instanceof Long ? (Long) a : ((Integer) a).longValue();
			Long bl = b instanceof Long ? (Long) b : ((Integer) b).longValue();

			if (c == '+')
				return al + bl;
			if (c == '-')
				return al - bl;
			if (c == '*')
				return al * bl;
			if (c == '%')
				return al % bl;
			if (c == '^')
				return Math.pow(al, bl);
			if (c == '/') {
				if (bl == 0L)
					throw new IllegalArgumentException("Divide by zero");
				return al / bl;
			}

			throw new IllegalArgumentException("Invalid operator " + c);
		}

		// one or both are doubles

		Double ad = a instanceof Double ? (Double) a
				: (a instanceof Long ? ((Long) a).doubleValue() : ((Integer) a).doubleValue());
		Double bd = b instanceof Double ? (Double) b
				: (b instanceof Long ? ((Long) b).doubleValue() : ((Integer) b).doubleValue());

		if (c == '+')
			return ad + bd;
		if (c == '-')
			return ad - bd;
		if (c == '*')
			return ad * bd;
		if (c == '%')
			return ad % bd;
		if (c == '^')
			return Math.pow(ad, bd);
		if (c == '/') {
			if (bd == 0d)
				throw new IllegalArgumentException("Divide by zero");
			return ad / bd;
		}
		throw new IllegalArgumentException("Invalid operator " + c);
	}
}