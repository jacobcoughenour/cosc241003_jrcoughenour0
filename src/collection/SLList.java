package collection;

/**
 * Singly Linked List
 * @author jacob
 */
public class SLList {

	protected SLListNode head;
	protected SLListNode tail;
	protected int size;

	/**
	 * SLList Constructor
	 */
	public SLList() {
		head = null;
		tail = null;
		size = 0;
	}

	/**
	 * Clear all elements from list
	 */
	public void clear() {
		head = null;
		tail = null;
		size = 0;
	}

	/**
	 * @return true if list is empty
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * @return current number of elements
	 */
	public int size() {
		return size;
	}

	/**
	 * Appends given elements to the tail
	 * @param elements elements to append
	 */
	public void append(Object... elements) {
		for (Object e : elements)
			this.append(e);
	}

	/**
	 * Appends given element to the tail
	 * @param element element to append
	 */
	public void append(Object element) {
		SLListNode node = new SLListNode(element);

		if (isEmpty())
			head = node;
		else
			tail.next = node;

		tail = node;
		size++;
	}

	/**
	 * Inserts the given elements at the head
	 * @param elements elements to insert
	 */
	public void insert(Object... elements) {
		for (Object e : elements)
			this.insert(e);
	}

	/**
	 * Inserts the given element at the head
	 * @param element element to insert
	 */
	public void insert(Object element) {

		// insert element at head
		head = new SLListNode(element, head);

		// if empty, also set the tail
		if (size == 0)
			tail = head;

		size++;
	}

	/**
	 * Removes the given elements from the list
	 */
	public void remove(Object... elements) {
		for (Object e : elements)
			this.remove(e);
	}

	/**
	 * Removes the given element from the list
	 * @param element element to remove
	 * @return true if element was removed
	 */
	public boolean remove(Object element) {

		if (size == 0)
			return false;

		SLListNode node = head;

		if (node.data.equals(element)) {
			head = head.next;
			if (size == 1)
				tail = head;

			size--;
			return true;
		}

		while(node.next != null) {

			if (node.next.data.equals(element)) {

				// if tail == element
				if (node.next == tail)
					// set new tail to prev node
					tail = node;

				node.next = node.next.next;

				size--;
				return true;
			}

			node = node.next;
		}

		return false;
	}

	/**
	 * @return String representation of the current elements in this list
	 */
	@Override
	public String toString() {
		String s = "[";
		SLListNode node = head;

		while (node != null) {
			s += node.data.toString();
			if (node.next != null)
				s += ", ";
			node = node.next;
		}

		return s + "]";
	}
}