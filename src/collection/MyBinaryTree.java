package collection;

/**
 * Binary Tree
 *
 * @author jacob
 */
public abstract class MyBinaryTree<T> {

	protected MyBinaryTreeNode<T> root;

	/**
	 * MyBinaryTree Constructor
	 */
	public MyBinaryTree() {
		root = null;
	}

	/**
	 * Clear all elements from tree
	 */
	public void clear() {
		root = null;
	}

	/**
	 * @return true if tree is empty
	 */
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * @return total number of elements
	 */
	public int size() {
		return getSize(root);
	}

	/**
	 * Gets the number of nodes in sub-tree
	 *
	 * @param node sub-tree root
	 * @return number of nodes in sub-tree
	 */
	protected static int getSize(MyBinaryTreeNode node) {
		if (node == null)
			return 0;
		return 1 + getSize(node.left) + getSize(node.right);
	}

	/**
	 * Traversal Order
	 */
	public enum Order {
		/**
		 * <pre>
		 * Left -> Root -> Right
		 *     4
		 *    / \
		 *   2   5
		 *  / \
		 * 1   3
		 * </pre>
		 */
		Inorder,
		/**
		 * <pre>
		 * Left -> Right -> Root
		 *     1
		 *    / \
		 *   2   5
		 *  / \
		 * 3   4
		 * </pre>
		 */
		Postorder,
		/**
		 * <pre>
		 * Root -> Left -> Right
		 *     5
		 *    / \
		 *   3   4
		 *  / \
		 * 1   2
		 * </pre>
		 */
		Preorder
	}

	/**
	 * Converts tree to an array
	 *
	 * @returns Object array of nodes in tree
	 */
	public Object[] toArray() {
		return toArray(Order.Inorder);
	}

	/**
	 * Converts tree to an array
	 *
	 * @param order Traversal order to append the nodes to the array
	 * @returns Object array of nodes in tree
	 */
	public Object[] toArray(Order order) {
		Object[] array = new Object[size()];
		populateArray(root, array, 0, order);
		return array;
	}

	/**
	 * Internal recursive array population for toArray()
	 *
	 * @param node  current sub-tree root
	 * @param array array to write to
	 * @param index of the array to write to
	 * @param order order to traverse the tree
	 * @return new index value
	 */
	private int populateArray(MyBinaryTreeNode<T> node, Object[] array, int index, Order order) {

		if (order != Order.Preorder && node.left != null)
			index = populateArray(node.left, array, index, order);

		if (order == Order.Postorder && node.right != null)
			index = populateArray(node.right, array, index, order);

		array[index] = node.data;
		index++;

		if (order == Order.Preorder && node.left != null)
			index = populateArray(node.left, array, index, order);

		if (order != Order.Postorder && node.right != null)
			index = populateArray(node.right, array, index, order);

		return index;
	}

	/**
	 * @return String representation of the tree
	 */
	@Override
	public String toString() {
		return toString(Order.Inorder);
	}

	/**
	 * Converts tree to string in the given order
	 *
	 * @param order traversal order for the nodes
	 * @return String representation of the tree
	 */
	public String toString(Order order) {
		return toString(order, ", ");
	}

	/**
	 * Converts tree to string in the given order
	 *
	 * @param order     traversal order for the nodes
	 * @param separator separator to add between values
	 * @return String representation of the tree
	 */
	public String toString(Order order, String separator) {
		if (root == null)
			return "";

		String s = "";
		Object[] elements = toArray(order);

		for (int i = 0; i < elements.length - 1; i++)
			s += elements[i] + separator;

		return s + elements[elements.length - 1];
	}

}