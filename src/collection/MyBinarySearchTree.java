package collection;

/**
 * Binary Search Tree
 *
 * @author jacob
 */
public class MyBinarySearchTree<T extends Comparable<T>> extends MyBinaryTree<T> {

	/**
	 * Binary Search Tree constructor
	 */
	public MyBinarySearchTree() {
		super();
	}

	/**
	 * Insert multiple values into tree
	 *
	 * @param values values to insert
	 */
	public void insert(T... values) {
		for (T value : values)
			insert(value);
	}

	/**
	 * Insert value into tree
	 *
	 * @param value value to insert
	 */
	public void insert(T value) {
		root = insertNode(root, value);
	}

	/**
	 * Recursive insertion
	 *
	 * @param node  sub-tree root node
	 * @param value value to insert
	 */
	private MyBinaryTreeNode<T> insertNode(MyBinaryTreeNode<T> node, T value) {
		if (node == null)
			return new MyBinaryTreeNode<T>(value);

		int diff = node.data.compareTo(value);

		if (diff > 0)
			node.left = insertNode(node.left, value);

		else if (diff < 0)
			node.right = insertNode(node.right, value);

		return node;
	}

	/**
	 * @return if tree contains given value
	 * @param value value to search for
	 */
	public boolean contains(T value) {

		MyBinaryTreeNode<T> node = root;

		while (node != null) {

			int diff = node.data.compareTo(value);

			if (diff == 0)
				return true;

			node = diff > 0 ? node.left : node.right;
		}

		return false;
	}

	/**
	 * removes given values from tree
	 *
	 * @param values values to remove
	 */
	public void remove(T... values) {
		for (T value : values)
			remove(value);
	}

	/**
	 * remove given value from tree
	 *
	 * @param value value to remove
	 */
	public void remove(T value) {
		root = removeNode(root, value);
	}

	/**
	 * Recursive removal
	 *
	 * @param node  sub-tree root node
	 * @param value value to remove
	 */
	private MyBinaryTreeNode<T> removeNode(MyBinaryTreeNode<T> node, T value) {

		if (node == null)
			return null;

		int diff = node.data.compareTo(value);

		if (diff == 0) {

			// has 2 children
			if (node.left != null && node.right != null) {
				node.data = getMin(node.right);
				node.right = removeNode(node.right, node.data);
				return node;
			}

			// has 1 child
			if (node.right != null)
				return node.right;
			if (node.left != null)
				return node.left;

			// is leaf
			return null;
		}

		if (diff > 0)
			node.left = removeNode(node.left, value);
		else
			node.right = removeNode(node.right, value);

		return node;
	}

	/**
	 * @return smallest value in tree
	 */
	public T getMin() {
		return (T) getMin(root);
	}

	/**
	 * Recursive getMin
	 *
	 * @param node current sub-tree node
	 * @return smallest value in tree
	 */
	private T getMin(MyBinaryTreeNode<T> node) {
		return node.left == null ? node.data : getMin(node.left);
	}

	/**
	 * @return largest value in tree
	 */
	public T getMax() {
		return (T) getMax(root);
	}

	/**
	 * Recursive getMax
	 *
	 * @param node current sub-tree node
	 * @return largest value in tree
	 */
	private T getMax(MyBinaryTreeNode<T> node) {
		return node.right == null ? node.data : getMax(node.right);
	}

}