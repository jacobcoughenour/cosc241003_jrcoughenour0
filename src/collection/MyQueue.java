package collection;

import java.util.Iterator;

/**
 * Queue List
 * @author jacob
 */
public class MyQueue<T> implements MyCollection<T> {

	protected SLListNode<T> first;
	protected SLListNode<T> last;
	protected int size;

	/**
	 * MyQueue default constructor
	 */
	public MyQueue() {
		first = null;
		last = null;
		size = 0;
	}

	/**
	 * MyQueue constructor
	 * @param elements elements to initialize with
	 */
	public MyQueue(Object... elements) {
		size = 0;

		// add all the elements to it
		for (Object e : elements)
			this.insertBack(e);
	}

	/**
	 * Inserts the given element to the end of the queue
	 * @param element element to insert
	 */
	public void insertBack(Object element) {
		if (element == null)
			return;

		if (size == 0) {
			first = last = new SLListNode(element);
			size++;
			return;
		}

		last.next = new SLListNode(element);
		last = last.next;

		size++;
	}

	/**
	 * Removes the element at the front and returns it
	 * @return the element removed from the front
	 */
	public Object removeFront() {
		if (size == 0)
			return null;

		Object e = first.data;

		first = first.next;
		size--;

		if (size == 0)
			last = null;

		return e;
	}

	/**
	 * @return The number of elements in the queue
	 */
	public int size() {
		return size;
	}

	/**
	 * @return The element at the beginning of the queue
	 */
	public Object first() {
		if (first == null)
			return null;
		return first.data;
	}

	/**
	 * @return The element at the end of the queue
	 */
	public Object last() {
		if (last == null)
			return null;
		return last.data;
	}

	/**
	 * @return String representation of the current elements in this queue
	 */
	@Override
	public String toString() {
		String s = "[";
		SLListNode node = first;

		while (node != null) {
			s += node.data.toString();
			if (node.next != null)
				s += ", ";
			node = node.next;
		}

		return s + "]";
	}

	public Iterator<T> iterator() {
		return new QueueIterator();
	}

	private class QueueIterator implements Iterator<T> {

		SLListNode cur;

		public QueueIterator() {
			cur = first;
		}

		public boolean hasNext() {
			return cur != null;
		}

		public T next() {
			Object data = cur.data;
			cur = cur.next;
			return (T) data;
		}

	}
}